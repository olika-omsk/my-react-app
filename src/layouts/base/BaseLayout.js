import React from 'react';
import PropTypes from 'prop-types';

import Header from './components/header/Header';
import Nav from './components/nav/Nav';

import './style.css';

const user = "Johny";

export default class BaseLayout extends React.Component {
  render() {
    return (
      <React.Fragment>
        <Header username={user}/>
        <main className='main'>
          <Nav className='main__nav'/>
          <section className='main__content'>
            {this.props.children}
          </section>
        </main>
      </React.Fragment>
    );
  };
};

BaseLayout.propTypes = {
  children: PropTypes.node.isRequired
};
