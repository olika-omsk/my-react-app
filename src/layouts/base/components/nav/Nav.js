import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from "react-router-dom";

import './style.css';
// import todo_icon from "./Todo.png";
// import done_icon from "./Done.png";

export default class Nav extends React.Component {
  render() {
    const { className } = this.props;

    return (
      <nav className={`nav${` ${className}`}`}>
          <ul className='nav__list'>
            <li className='nav__point'>
              <NavLink exact to={'/'} className='nav__link' activeClassName='nav__link_active'>
                {/*<img src={todo_icon} className="nav__icon"/><span className="nav__item"> To Do</span>*/}
                <div className="nav__icon nav__icon_todo"/><span className="nav__item"> To Do</span>
              </NavLink>
            </li>
            <li className='nav__point'>
              <NavLink to={'/done'} className='nav__link' activeClassName='nav__link_active'>
                {/*<img src={done_icon} className="nav__icon"/><span className="nav__item"> Done</span>*/}
                <div className="nav__icon nav__icon_done"/><span className="nav__item"> Done</span>
              </NavLink>
            </li>
          </ul>
      </nav>
    );
  };
};

Nav.propTypes = {
  className: PropTypes.string
};

Nav.defaultProps = {
  className: ''
};
