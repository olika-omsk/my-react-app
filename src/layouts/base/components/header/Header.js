import React from 'react';
import logo from './logo.png';

import './style.css';
import PropTypes from "prop-types";
import Task from "../../../../components/task/Task";

export default class Header extends React.Component {
  render() {
    return (
      <header className='header'>
        <div className='header__content'>
          <a className='header__logo' href='/'><img src={logo} className="App-logo" alt="Eise Tasks"/></a>
            <span className='header__user'>{this.props.username}</span>
        </div>
      </header>
    );
  };
};

Task.propTypes = {
    username: PropTypes.string
};

Task.defaultProps = {
    username: 'Johny'
};