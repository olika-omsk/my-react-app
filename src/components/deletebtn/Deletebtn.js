import React from 'react';
// import PropTypes from 'prop-types';
import './style.css';
import Button from '../button/Button';

export default class Deletebtn extends React.Component {

    render() {
        const { className, taskID, onClick } = this.props;

        return (
            <Button
                className={`button_square basket${className ? ` ${className}` : ''}`}
                taskID={taskID}
                onClick={onClick}
            />
        )}
};

/*Deletebtn.defaultProps = {
    className: '',
    taskID: '222',
    onClick: () => {}
};

Deletebtn.propTypes = {
    className: PropTypes.string,
    taskID: PropTypes.string,
    onClick: PropTypes.func
};*/