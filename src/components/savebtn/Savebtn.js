import React from 'react';
// import PropTypes from 'prop-types';
import './style.css';

import Button from "../button/Button";

export default class Savebtn extends React.Component {
    render() {
        const { className, taskID, onClick } = this.props;

        return (
            <Button
                className={`button_square save${className ? ` ${className}` : ''}`}
                taskID={taskID}
                onClick={onClick}
            />
        )}
};

/*Editbtn.defaultProps = {
    className: '',
    taskID: '3333',
    onClick: () => {}
};

Editbtn.propTypes = {
    className: PropTypes.string,
    taskID: PropTypes.string,
    onClick: PropTypes.func
};*/