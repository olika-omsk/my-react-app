import React from 'react';
import PropTypes from 'prop-types';
import './style.css';

export default class Task extends React.Component {
  render() {
    const { className } = this.props;

    return (
      <div className={`task${className ? ` ${className}` : ''}`}>
        <div className="task__content">{this.props.children}</div>
      </div>
    );
  };
};

Task.propTypes = {
  children: PropTypes.node.isRequired
};
