import React from 'react';
import './style.css';
import PropTypes from "prop-types";

export default class Button extends React.Component {
    render() {
        const { className, taskID, onClick, value, type, disabled } = this.props;

        return (
            <button
                className={`button${className ? ` ${className}` : ''}`}
                taskid={taskID}
                onClick={onClick}
                type={type}
                disabled={disabled}
            >
                {value}
            </button>
        )}
};

Button.defaultProps = {
    className: '',
    taskid: '',
    onClick: () => {},
    value: '',
    type: 'button',
    disabled: false
};

Button.propTypes = {
    className: PropTypes.string,
    taskid: PropTypes.string,
    onClick: PropTypes.func,
    value: PropTypes.string,
    type: PropTypes.string,
    disabled: PropTypes.bool
};