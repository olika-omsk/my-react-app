import React from 'react';
import PropTypes from 'prop-types';
import './style.css';

export default class Textinput extends React.Component {
    render() {
        const { className, value, onChange } = this.props;

        return (
                <input className={`text-input${className ? ` ${className}` : ''}`} defaultValue={value} onChange={onChange}/>
        )}
};

Textinput.propTypes = {
    body: PropTypes.string,
    value: PropTypes.string,
    onChange: PropTypes.func
};


Textinput.defaultProps = {
    body: '',
    value: '',
    onChange: () => {}
};
