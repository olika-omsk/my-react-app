import React from 'react';
import radiobtn from './Oval.png';
import radiointro from './OvalIntro.png';
import './style.css';

export default class Radio extends React.Component {
    render() {
        const { className, taskID, onClick } = this.props;

        return (
            <div className={`radio${className ? ` ${className}` : ''}`}>
                <img alt=""
                     src={radiobtn}
                     className="radio__img"
                     taskid={taskID}
                     onClick={onClick}
                />
                <img alt=""
                     src={radiointro}
                     className="radio__img_selected"
                     taskid={taskID}
                     onClick={onClick}
                />
            </div>
    )}
};


