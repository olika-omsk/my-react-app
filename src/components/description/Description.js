import React from 'react';
import PropTypes from 'prop-types';
import './style.css';

export default class Description extends React.Component {
    render() {
        const { className, onClick, onMouseOver } = this.props;

        return (
                <span
                    className={`description-text${className ? ` ${className}` : ''}`}
                    onMouseOver={onMouseOver}
                    onClick={onClick}
                >{this.props.body}</span>
        )}
};

Description.propTypes = {
    body: PropTypes.string,
    className: PropTypes.string,
    onClick: PropTypes.func,
    onMouseOver: PropTypes.func
};


Description.defaultProps = {
    body: '',
    className: '',
    onClick: () => {},
    onMouseOver: () => {}
};
