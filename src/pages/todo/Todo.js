import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Task from '../../components/task/Task';
import Radio from "../../components/radiobtn/Radio";
import Editbtn from "../../components/editbtn/Editbtn";
import Savebtn from "../../components/savebtn/Savebtn";
import Deletebtn from "../../components/deletebtn/Deletebtn";
import Description from "../../components/description/Description";
import Textinput from "../../components/textinput/Textinput";
import FormField from "../../components/formField/FormField";
import Button from '../../components/button/Button';
import './style.css';
import getTaskList from '../../actions/taskList/getTaskList';
import setTaskList from '../../actions/taskList/setTaskList';
import setTaskEdit from '../../actions/taskList/setTaskEdit';
import saveTask from '../../actions/taskList/saveTask';
import deleteTask from '../../actions/taskList/deleteTask';
import doneTask from '../../actions/taskList/doneTask';

class Todo extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            value: '',
            editTaskValue:''
        };
    };

    componentDidMount() {
        this.props.getTaskList()
    };

    handleChoose = (event) => {
        console.log("event RADIO:");
        console.log(event.target.attributes.taskid.value);
        this.props.doneTask(event.target.attributes.taskid.value);
    };

    handleEdit = (event) => {
        let tex = '';
        this.props.taskList.map(task => {
            if(task.id == event.target.attributes.taskid.value) {
                this.props.setTaskEdit(task.id);
                tex = task.body;
            }
        });
        console.log('EDIT button input');
        console.log(event.target);

        this.setState({
                  editTaskValue: tex
        });
    };

    handleTaskChange = (event) => {
        this.setState({
            editTaskValue: event.target.value
        });
    };

    handleSave = (event) => {
        event.preventDefault();
        this.props.saveTask(event.target.attributes.taskid.value, this.state.editTaskValue);
        this.setState({
            editTaskValue: ''
        });
    };

    handleDelete = (event) => {
        console.log("event delete:");
        console.log(event.target.attributes.taskid.value);
        this.props.deleteTask(event.target.attributes.taskid.value, 'task');
    };

    handleChange = (event) => {
        this.setState({
            value: event.target.value
        });
    };

    handleSubmit = (event) => {
        event.preventDefault();
        this.setState({
            value: ''
        });

        this.props.setTaskList(this.state.value);
        console.log('set task list!');
    };

    renderList = () => {
      return this.props.taskList.map((item, index) => {
          console.log("edit:", item.edit);
          let taskContent;
          if(item.edit){
              taskContent = <React.Fragment>
                                <Textinput
                                    className="todo-task__text-input"
                                    value={item.body}
                                    onChange={this.handleTaskChange}
                                />
                                <Savebtn
                                  className="todo-task__save-btn"
                                  taskID={item.id}
                                  onClick={this.handleSave}
                                />
                            </React.Fragment>
          }else{
              taskContent =<React.Fragment>
                          <Description
                              body={item.body}
                              className="todo-task__description"
                          />
                          < Editbtn
                              className="todo-task__edit-btn"
                              taskID={item.id}
                              onClick={this.handleEdit}
                          />
                        </React.Fragment>
          }
      return (
        <Task key={index} taskID={item.id} className="todo-task">
          <Radio
              className="todo-task__radio-btn"
              taskID={item.id}
              onClick={this.handleChoose}
          />
            {taskContent}
          <Deletebtn
              className='todo-task__basket-btn'
              taskID={item.id}
              onClick={this.handleDelete}
          />
        </Task>
      );
    });
  };

  render() {
    return (
      <React.Fragment>
          <form
              className='form'
              onSubmit={this.handleSubmit}
          >
              <FormField
                  className='form__field'
                  value={this.state.value}
                  placeholder='Type your new task'
                  onChange={this.handleChange}
              />
              <Button
                  className='form__button button_oval'
                  type='submit'
                  value='+ Create'
                  disabled={this.state.value.length === 0}
              />
          </form>
          {this.renderList()}
      </React.Fragment>
    );
  };
}

const mapStateToProps = (state) => ({
    taskList: state.taskListReducer.taskList
});

const mapDispatchToProps = (dispatch) => ({
    getTaskList: bindActionCreators(getTaskList, dispatch),
    setTaskList: bindActionCreators(setTaskList, dispatch),
    setTaskEdit: bindActionCreators(setTaskEdit, dispatch),
    saveTask: bindActionCreators(saveTask, dispatch),
    deleteTask: bindActionCreators(deleteTask, dispatch),
    doneTask: bindActionCreators(doneTask, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Todo);
