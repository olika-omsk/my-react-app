import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Task from '../../components/task/Task';
import Description from "../../components/description/Description";
import Deletebtn from "../../components/deletebtn/Deletebtn";
import checkimg from './V.png';
import './style.css';
import getReadyTaskList from '../../actions/taskList/getReadyTaskList';
import deleteTask from "../../actions/taskList/deleteTask";

class Done extends React.Component {
     componentDidMount() {
        this.props.getReadyTaskList();
    }


    handleDelete = (event) => {
        console.log("event delete:");
        console.log(event.target.attributes.taskid.value);
        this.props.deleteTask(event.target.attributes.taskid.value, 'ready_task');
    };

  renderList = () => {
    // return list.data.map((item, index) => {
      return this.props.readyTaskList.map((item, index) => {
      return (
        <Task key={index} title={item.title} className="done-task" >
            <img src={checkimg} alt="" className="done-task__check-image"/>
            <Description body={item.body} className="done-task__description"/>
            <Deletebtn
                className='done-task__basket-btn'
                taskID={item.id}
                onClick={this.handleDelete}/>
        </Task>
      );
    });
  };

  render() {
    return (
      <React.Fragment>
        {this.renderList()}
      </React.Fragment>
    );
  };
};

const mapStateToProps = (state) => ({
    readyTaskList: state.readyTaskListReducer.readyTaskList
});

const mapDispatchToProps = (dispatch) => ({
    getReadyTaskList: bindActionCreators(getReadyTaskList, dispatch),
    deleteTask: bindActionCreators(deleteTask, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Done);
