import * as types from '../actions/taskList/actionTypes';

const initialState = {
    taskList: [],
    error: null
};

export default (state = initialState, action) => {
    // console.log("state in taskListReducer:");
    // console.log(state);
    switch(action.type) {
        case types.GET_TASK_LIST_SUCCESS: {
            return {
                ...state,
                taskList: action.taskList,
                error: null
            }
        }
        case types.GET_TASK_LIST_ERROR: {
            return {
                ...state,
                taskList: [],
                error: action.error
            }
        }
        case types.SET_TASK_LIST_SUCCESS: {
            return {
                ...state,
                taskList: [action.task, ...state.taskList],
                error: null
            }
        }
        case types.SET_TASK_LIST_ERROR: {
            console.log('set task list error');
            return state;
        }
        case types.SET_TASK_EDIT: {
            let newList = [...state.taskList];
            newList.map((task) => {
                if(task.id === action.taskID) {
                    task.edit = 1;
                }
                else {
                    task.edit = 0;
                }
            });
            return {
                ...state,
                taskList: newList,
                error: null
            }
        }
        case types.SAVE_TASK_SUCCESS: {
            let newList = [...state.taskList];
            newList.map((task) => {
                if(task.id === action.taskID) {
                    task.body = action.taskBody;
                }
                task.edit = 0;
            });
            return {
                ...state,
                taskList: newList,
                error: null
            }
        }
        case types.SAVE_TASK_ERROR: {
            console.log('save task error');
            return state;
        }
        case types.DELETE_TASK_SUCCESS: {
            let newList = [...state.taskList];
            newList.map((task, index) => {
                if(task && (task.id === action.taskID)) {
                    newList.splice(index,1);
                }
            });
            // console.log("new list:");
            // console.log(newList);
            return {
                ...state,
                taskList: newList,
                error: null
            }
        }
        case types.DELETE_TASK_ERROR: {
            console.log('delete task error');
            return state;
        }
        case types.DONE_TASK_SUCCESS: {
            let newList = [...state.taskList];
            let taskMove = {};
            newList.map((task, index) => {
                if(task && (task.id === action.taskID)) {
                    newList.splice(index,1);
                    taskMove = task;
                }
            });
            console.log("new list:");
            console.log(newList);
            console.log("task move:");
            console.log(taskMove);

            console.log("state:");
            console.log(state);
            return {
                ...state,
                taskList: newList,
                readyTaskList: [taskMove, ...state.readyTaskList],
                error: null
            }
        }
        case types.DONE_TASK_ERROR: {
            console.log('done task error');
            return state;
        }
        default: {
            return state;
        }
    }
}