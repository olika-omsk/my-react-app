import * as types from '../actions/taskList/actionTypes';

const initialState = {
    readyTaskList: [],
    error: null
};

export default (state = initialState, action) => {
    switch(action.type) {
        case types.GET_READY_TASK_LIST_SUCCESS: {
            return {
                ...state,
                readyTaskList: action.readyTaskList,
                error: null
            }
        }
        case types.GET_READY_TASK_LIST_ERROR: {
            return {
                ...state,
                readyTaskList: [],
                error: action.error
            }
        }
        case types.DELETE_READY_TASK_SUCCESS: {
            let newList = [...state.readyTaskList];
            newList.map((task, index) => {
                if(task && (task.id === action.taskID)) {
                    newList.splice(index,1);
                }
            });
            // console.log("new list:");
            // console.log(newList);
            return {
                ...state,
                readyTaskList: newList,
                error: null
            }
        }
        case types.DELETE_READY_TASK_ERROR: {
            console.log('delete task error');
            return state;
        }
        default: {
            return state;
        }
    }
}