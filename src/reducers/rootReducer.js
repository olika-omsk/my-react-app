import { combineReducers } from 'redux';

import taskListReducer from './taskListReducer';
import readyTaskListReducer from './readyTaskListReducer';

export default (state = {}, action) => {
    return combineReducers({
        taskListReducer,
        readyTaskListReducer
    })(state, action);

}