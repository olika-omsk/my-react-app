function checkStatus(response){
    if(response.status >= 200 && response.status < 300) {
        return response;
    }
    throw new Error(response.statusText);
}

export function get(url){
    return fetch(url, {
        method: 'GET',
        headers: new Headers({
            'Accept': 'application/json',
            'Content-Type': 'application/json;charset=utf-8'
            // 'Access-Control-Allow-Origin': '*'
        })
    })
        .then((response) => checkStatus(response))
        .then((response) => response.json())
        .catch((error) => {
            return error;
        })
}

export function set(list_id, newtask){
    console.log('newtask in set:');
    console.log(newtask);

    let maxid1 = new Promise((resolve, reject) => {
        if (1===1) {
            return resolve('id6');
        } else {
            return reject('Error in SET')
        }
    });
    return maxid1;

    /*let modelurl = '';

    switch (list_id) {
        case 'task':
            modelurl = 'http://react7bits.dev/php/model.php';
            break;
        case 'ready_task':
            modelurl = 'http://react7bits.dev/php/model_ready.php';
            break;
    }
    return fetch(modelurl, {
        method: 'POST',
        headers: new Headers({
            "content-type": "application/x-www-form-urlencoded"
        }),
        body: JSON.stringify(newposts)
    })
        .then((response) => console.log(response))
        // .then((response) => checkStatus(response))
        // .then((response) => response.json())
        .then((result) => console.log(result))
        .catch((error) => {
            return error;
        });
*/
}

export function save(id, body){
    return new Promise((resolve, reject) => {
        if ((id===id) && (body===body)) {
            return resolve(true);
        } else {
            return reject('Error in SAVE')
        }
    });
}

export function delTask(id, list_id){

    /*let modelurl = '';

    switch (list_id) {
        case 'task':
            modelurl = 'http://react7bits.dev/php/model.php';
            break;
        case 'ready_task':
            modelurl = 'http://react7bits.dev/php/model_ready.php';
            break;
    }
    return fetch(modelurl, {
        method: 'POST',
        headers: new Headers({
            "content-type": "application/x-www-form-urlencoded"
        }),
        body: JSON.stringify(newposts)
    })
        .then((response) => console.log(response))
        // .then((response) => checkStatus(response))
        // .then((response) => response.json())
        .then((result) => console.log(result))
        .catch((error) => {
            return error;
        });
*/

    return new Promise((resolve, reject) => {
        if ((id===id) && (list_id===list_id)) {
            return resolve(true);
        } else {
            return reject('Error in SAVE')
        }
    });
}

export function doTask(id){

    /*let modelurl = '';

    switch (list_id) {
        case 'task':
            modelurl = 'http://react7bits.dev/php/model.php';
            break;
        case 'ready_task':
            modelurl = 'http://react7bits.dev/php/model_ready.php';
            break;
    }
    return fetch(modelurl, {
        method: 'POST',
        headers: new Headers({
            "content-type": "application/x-www-form-urlencoded"
        }),
        body: JSON.stringify(newposts)
    })
        .then((response) => console.log(response))
        // .then((response) => checkStatus(response))
        // .then((response) => response.json())
        .then((result) => console.log(result))
        .catch((error) => {
            return error;
        });
*/

    return new Promise((resolve, reject) => {
        if (id===id)  {
            return resolve(true);
        } else {
            return reject('Error in DONE')
        }
    });
}

/*
export function getTaskText(id){
    return new Promise((resolve, reject) => {
        if (id===id) {
            return resolve('text ...');
        } else {
            return reject('Error in SAVE')
        }
    });
}*/
