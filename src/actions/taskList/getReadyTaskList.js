import {get} from '../../fetcher/fetcher';
import * as types from '../taskList/actionTypes';

export default function getReadyTaskList() {
    return (dispatch) => {
        return get ('mockapi/getReadyTaskList.json')
        // return get ('http://react7bits.dev/json/getTaskList.json')
            .then(response => {
                dispatch({
                    type: types.GET_READY_TASK_LIST_SUCCESS,
                    readyTaskList: response.data
                })
            })
            .catch(error => {
                dispatch({
                    type: types.GET_READY_TASK_LIST_ERROR,
                    error: error
                })
            })
    }
}
