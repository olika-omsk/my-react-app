import {get} from '../../fetcher/fetcher';
import * as types from './actionTypes';

export default function getTaskList() {
    return (dispatch) => {
        return get ('mockapi/getTaskList.json')
        // return get ('http://react7bits.dev/json/getTaskList.json')
        // return get ('http://jsonplaceholder.typicode.com/posts?_limit=5')
            .then(response => {
                console.log('response in getTaskList.js:');
                console.log(response);
                response.data.map(a => {a['edit'] = 0;});
                console.log('with edit flags:');
                console.log(response.data);
                dispatch({
                    type: types.GET_TASK_LIST_SUCCESS,
                    taskList: response.data
                    // taskList: response
                })
            })
            .catch(error => {
                console.log('CATCH in getTaskList');
                console.log(error);
                dispatch({
                    type: types.GET_TASK_LIST_ERROR,
                    error: error
                })
            })
    }
}
