import {set} from '../../fetcher/fetcher';
import * as types from './actionTypes';

export default function setTaskList(taskbody) {
    let task = {
                'body': taskbody
            };

    return (dispatch) => {
        return set ('task', task)
            .then((id) => {
                console.log('maxid as response in setTaskList:');
                console.log(id);
                dispatch({
                    type: types.SET_TASK_LIST_SUCCESS,
                    task: {
                        id: id,
                        ...task,
                        edit: 0
                    }
                })
            })
            .catch(error => {
                console.log('CATCH in setTaskList');
                console.log(error);
                dispatch({
                    type: types.SET_TASK_LIST_ERROR,
                    error: error
                })
            })
    }
}
