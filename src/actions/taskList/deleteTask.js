import {delTask} from '../../fetcher/fetcher';
import * as types from './actionTypes';

export default function deleteTask(id, list_id) {
    console.log("DELETE!!!");

    return (dispatch) => {
        return delTask(id, list_id)
            .then(() => {
                console.log('deleted from list:');
                console.log(list_id);
                if(list_id === 'task') {
                    dispatch({
                        type: types.DELETE_TASK_SUCCESS,
                        taskID: id
                    })
                }else{
                    dispatch({
                        type: types.DELETE_READY_TASK_SUCCESS,
                        taskID: id
                    })
                }
            })
            .catch(error => {
                console.log('CATCH in deleteTask');
                console.log(error);
                dispatch({
                    type: types.DELETE_TASK_ERROR,
                    error: error
                })
            })
    }
}
