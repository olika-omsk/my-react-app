import {doTask} from '../../fetcher/fetcher';
import * as types from './actionTypes';

export default function doneTask(id) {
    console.log("DONE!!!");

    return (dispatch) => {
        return doTask(id)
            .then((res) => {
                console.log('done task:');
                console.log(res);
                    dispatch({
                        type: types.DONE_TASK_SUCCESS,
                        taskID: id
                    })
            })
            .catch(error => {
                console.log('CATCH in deleteTask');
                console.log(error);
                dispatch({
                    type: types.DONE_TASK_ERROR,
                    error: error
                })
            })
    }
}
