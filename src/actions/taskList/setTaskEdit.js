import * as types from './actionTypes';

export default function setTaskEdit(taskID) {

    return (dispatch) => {
        // return (
            dispatch({
                type: types.SET_TASK_EDIT,
                taskID: taskID
            }
            // )
        )
    }
}
