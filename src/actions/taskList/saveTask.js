import {save} from '../../fetcher/fetcher';
import * as types from './actionTypes';

export default function saveTask(id, body) {
    console.log("BODY::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::;");
    console.log(body);

    return (dispatch) => {
        return save (id, body)
            .then(() => {
                console.log('saved');
                dispatch({
                    type: types.SAVE_TASK_SUCCESS,
                    taskID: id,
                    taskBody: body
                })
            })
            .catch(error => {
                console.log('CATCH in saveTask');
                console.log(error);
                dispatch({
                    type: types.SAVE_TASK_ERROR,
                    error: error
                })
            })
    }
}
